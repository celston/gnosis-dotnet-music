﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gnosis.Music.Spotify;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Auth;
using SpotifyAPI.Web.Enums;
using SpotifyAPI.Web.Models;

namespace Gnosis.Music.SpotifyAuthConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        static async Task MainAsync(string[] args)
        {
            SpotifyWebAPI spotify;

            WebAPIFactory webApiFactory = new WebAPIFactory(
                "http://localhost",
                8000,
                Constants.ClientId,
                Scope.UserReadPrivate,
                TimeSpan.FromSeconds(20)
            );

            try
            {
                //This will open the user's browser and returns once
                //the user is authorized.
                spotify = await webApiFactory.GetWebApi();
                using (ILogger logger = new FileLogger())
                {
                    MixPlaylistUpdater mixPlaylistUpdater = new MixPlaylistUpdater(spotify, logger);
                    mixPlaylistUpdater.Update();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
