﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gnosis.Music
{
    public class Track
    {
        public string Artist { get; set; }
        public string Album { get; set; }
        public string Name { get; set; }
        public string Uri { get; set; }
        public double Popularity { get; set; }

        public Track Create(SpotifyLocalTrack track)
        {
            throw new NotImplementedException();
        }
    }
}
