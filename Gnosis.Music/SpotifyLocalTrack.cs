﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Gnosis.Music
{
    public class SpotifyLocalTrack
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string Artist { get; set; }
        public string Album { get; set; }
        public string Name { get; set; }
        public string Uri { get; set; }
        public bool IsLocal { get; set; }
    }
}