﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Gnosis.Music.LastFm
{
    public class UserRecentTrack
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string Artist { get; set; }
        public string Name { get; set; }
        public string Album { get; set; }
        public BsonDateTime Date { get; set; }
    }
}