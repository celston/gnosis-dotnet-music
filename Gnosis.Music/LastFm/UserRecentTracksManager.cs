﻿using System;
using System.Collections.Generic;
using System.Web.Helpers;
using Microsoft.CSharp.RuntimeBinder;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Gnosis.Music.LastFm
{
    public class UserRecentTracksManager
    {
        private readonly IWebClientFacadeFactory _webClientFacadeFactory;
        private readonly ILogger _logger;

        public UserRecentTracksManager(IWebClientFacadeFactory webClientFacadeFactory, ILogger logger)
        {
            _webClientFacadeFactory = webClientFacadeFactory;
            _logger = logger;
        }

        public UserRecentTracksManager(ILogger logger)
            : this(new WebClientFacadeFactory(3, logger), logger)
        {
        }

        private readonly Lazy<IMongoCollection<UserRecentTrack>> _lazyCollection = new Lazy<IMongoCollection<UserRecentTrack>>(() => Utility.Database.GetCollection<UserRecentTrack>("recenttracks"));

        private IMongoCollection<UserRecentTrack> Collection => _lazyCollection.Value;

        public int ComputeFrom(DateTime d)
        {
            TimeSpan diff = d - Constants.OriginDate;
            return (int)Math.Floor(diff.TotalSeconds);
        }

        public List<UserRecentTrack> GetLocal(DateTime fromDate)
        {
            var filter = Builders<UserRecentTrack>.Filter.Gt(x => x.Date, fromDate);

            return Collection.Find(filter).ToList();
        }

        public List<UserRecentTrack> GetLocalRandom(DateTime fromDate, int n)
        {
            var filter = Builders<UserRecentTrack>.Filter.Gt(x => x.Date, fromDate);

            int count = (int)Collection.Count(filter);
            List<UserRecentTrack> result = new List<UserRecentTrack>();
            Random random = new Random();
            for (int i = 0; i < n; i++)
            {
                result.AddRange(Collection.Find(filter).Skip(random.Next(count)).Limit(1).ToList());
            }

            return result;
        }

        public List<UserRecentTrack> GetRemote(DateTime fromDate)
        {
            int from = ComputeFrom(fromDate) + 1;
            
            int limit = 200;
            int page = 1;
            string urlFormat = "http://ws.audioscrobbler.com/2.0/?method=user.getRecentTracks&user={0}&api_key={1}&limit={2}&page={3}&format=json&from={4}";

            List<UserRecentTrack> result = new List<UserRecentTrack>();

            using (IWebClientFacade webClient = _webClientFacadeFactory.Create())
            {
                string url = string.Format(urlFormat, Constants.Username, Constants.ApiKey, limit, page, from);

                string json = webClient.DownloadString(url);
                dynamic data = Json.Decode(json);

                int totalPages = int.Parse(data.recenttracks["@attr"].totalPages);

                foreach (dynamic track in data.recenttracks.track)
                {
                    try
                    {
                        result.Add(CreateTrack(track));
                    }
                    catch (RuntimeBinderException)
                    {
                    }
                }

                for (page = 2; page <= totalPages; page++)
                {
                    url = string.Format(urlFormat, Constants.Username, Constants.ApiKey, limit, page, from);
                    json = webClient.DownloadString(url);
                    data = Json.Decode(json);

                    foreach (dynamic track in data.recenttracks.track)
                    {
                        try
                        {
                            result.Add(CreateTrack(track));
                        }
                        catch (RuntimeBinderException)
                        {
                        }
                    }
                }
            }

            return result;
        }

        public void RebuildDatabase()
        {
            Collection.DeleteMany(new BsonDocument());

            foreach (UserRecentTrack track in GetRemote(new DateTime(2000, 1, 1)))
            {
                Collection.InsertOne(track);
            }
        }

        public int RefreshDatabase()
        {
            UserRecentTrack doc = Collection.Find(new BsonDocument()).Sort(new BsonDocument("date", -1)).First();
            DateTime d = doc.Date.ToUniversalTime();

            var tracks = GetRemote(d);
            foreach (UserRecentTrack track in tracks)
            {
                Collection.InsertOne(track);
            }

            return tracks.Count;
        }

        private UserRecentTrack CreateTrack(dynamic track)
        {
            long uts = long.Parse(track.date.uts);
            DateTime date = Constants.OriginDate.AddSeconds(uts);
            return new UserRecentTrack()
            {
                Artist = track.artist["#text"],
                Name = track.name,
                Album = track.album["#text"],
                Date = new BsonDateTime(date)
            };
        }
    }
}