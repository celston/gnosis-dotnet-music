﻿namespace Gnosis.Music
{
    public interface IWebClientFacadeFactory
    {
        IWebClientFacade Create();
    }
}