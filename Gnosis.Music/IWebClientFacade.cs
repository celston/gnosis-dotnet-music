﻿using System;

namespace Gnosis.Music
{
    public interface IWebClientFacade : IDisposable
    {
        string DownloadString(string url);
    }
}