﻿namespace Gnosis.Music
{
    public class WebClientFacadeFactory : IWebClientFacadeFactory
    {
        private readonly int _maxRetries;
        private readonly ILogger _logger;

        public WebClientFacadeFactory(ILogger logger) 
            : this(1, logger)
        {
        }

        public WebClientFacadeFactory(int maxRetries, ILogger logger)
        {
            _maxRetries = maxRetries;
            _logger = logger;
        }

        public IWebClientFacade Create()
        {
            return new WebClientFacade(_maxRetries, _logger);
        }
    }
}