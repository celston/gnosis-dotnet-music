﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Gnosis.Music.Spotify
{
    public class WeightedRandomTrackSelector
    {
        public SimplifiedSpotifyTrack Select(string contextArtist, IList<SimplifiedSpotifyTrack> tracks)
        {
            if (!tracks.Any())
            {
                return null;
            }
            if (tracks.Count() == 1)
            {
                return tracks.First();
            }

            double totalPopularity = tracks.Select(x => AdjustPopularity(contextArtist, x)).Sum();
            Random random = new Random();
            double r = random.NextDouble() * totalPopularity;

            foreach (SimplifiedSpotifyTrack track in tracks)
            {
                double popularity = AdjustPopularity(contextArtist, track);
                if (r < popularity)
                {
                    return track;
                }

                r -= popularity;
            }

            return tracks.Last();

        }

        private double AdjustPopularity(string contextArtist, SimplifiedSpotifyTrack track)
        {
            var result = Math.Sqrt((track.Popularity) + 1);
            if (track.Artist.ToUpper() == contextArtist.ToUpper())
            {
                return result * 2;
            }

            return result;
        }
    }
}