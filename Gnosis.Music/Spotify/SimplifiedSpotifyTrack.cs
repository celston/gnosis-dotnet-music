﻿using SpotifyAPI.Web.Models;

namespace Gnosis.Music.Spotify
{
    public class SimplifiedSpotifyTrack
    {
        public string Artist { get; private set; }
        public string Album { get; private set; }
        public string Name { get; private set; }
        public string Uri { get; private set; }
        public bool IsExplicit { get; private set; }
        public int Popularity { get; private set; }

        public static SimplifiedSpotifyTrack Create(FullTrack track)
        {
            return new SimplifiedSpotifyTrack()
            {
                Artist = track.Artists[0].Name,
                Album = track.Album.Name,
                Name = track.Name,
                Uri = track.Uri,
                IsExplicit = track.Explicit,
                Popularity =  track.Popularity
            };
        }
    }
}