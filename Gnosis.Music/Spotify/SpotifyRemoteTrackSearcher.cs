﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gnosis.Music.Strings;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Enums;
using SpotifyAPI.Web.Models;

namespace Gnosis.Music.Spotify
{
    public class SpotifyRemoteTrackSearcher
    {
        private readonly SpotifyWebAPI _spotify;
        private readonly ILogger _logger;

        public SpotifyRemoteTrackSearcher(SpotifyWebAPI spotify, ILogger logger)
        {
            _spotify = spotify;
            _logger = logger;
        }

        private bool StringEquals(string a, string b)
        {
            return string.Equals(a, b, StringComparison.CurrentCultureIgnoreCase);
        }

        public List<SimplifiedSpotifyTrack> Search(int limit, string artist = null, string name = null, string album = null)
        {
            List<string> queries = new List<string>()
            {
                "NOT track:commentary"
            };

            string normalizedArtist = null;
            if (!string.IsNullOrWhiteSpace(artist))
            {
                normalizedArtist = NormalizeArtistName(artist);
                if (!StringEquals(artist, normalizedArtist))
                {
                    _logger.Debug($"Artist '{artist}' normalized to '{normalizedArtist}'");
                }
                
                queries.AddRange(Utility.GetLongestWords(normalizedArtist).Select(x => "artist:" + x));
            }

            if (!string.IsNullOrWhiteSpace(album))
            {
                string normalizedAlbum = StringProcessorRunner.Run(album, new IStringProcessor[]
                {
                    new RemoveLeadingTheProcessor(),
                    new SplitOnLeftParenthesisProcessor(),
                    new LatintoAsciiProcessor(),
                    new RemoveApostrophesProcessors()
                });
                if (!StringEquals(album, normalizedAlbum))
                {
                    _logger.Debug($"Album '{album}' normalized to '{normalizedAlbum}'");
                }
                
                queries.AddRange(Utility.GetLongestWords(normalizedAlbum).Select(x => "album:" + x));
            }

            if (!string.IsNullOrWhiteSpace(name))
            {
                string normalizedName = StringProcessorRunner.Run(name, new IStringProcessor[]
                {
                    new RemoveDotsProcessor(), 
                    new SplitOnDashProcessor(),
                    new SplitOnLeftParenthesisProcessor(),
                    new RemoveLeadingTheProcessor(),
                    new LatintoAsciiProcessor(),
                    new RemoveApostrophesProcessors()
                });
                if (!StringEquals(name, normalizedName))
                {
                    _logger.Debug($"Track name '{name}' normalized to '{normalizedName}'");
                }
                
                queries.AddRange(Utility.GetLongestWords(normalizedName).Select(x => "track:" + x));
            }

            string q = string.Join(" ", queries);
            int offset = 0;
            int subLimit = Math.Min(limit, 50);
            _logger.Debug($"SearchItems('{q}', {subLimit}, {offset})");
            SearchItem searchItem = _spotify.SearchItems(q, SearchType.Track, subLimit, offset);
            _logger.Debug($"searchItem.Tracks.Total = {searchItem.Tracks.Total}");

            List<SimplifiedSpotifyTrack> result = new List<SimplifiedSpotifyTrack>();

            while (result.Count < limit)
            {
                if (searchItem.Tracks?.Items == null)
                {
                    break;
                }

                int take = limit - result.Count;
                result.AddRange(searchItem.Tracks.Items.Select(SimplifiedSpotifyTrack.Create).Take(take));

                if (searchItem.Tracks.HasNextPage())
                {
                    offset += subLimit;
                    searchItem = _spotify.SearchItems(q, SearchType.Track, limit, offset);
                }
                else
                {
                    break;
                }
            }

            if (normalizedArtist == null)
            {
                return result;
            }

            List<SimplifiedSpotifyTrack> exactArtistMatches = result
                .Where(x => StringEquals(NormalizeArtistName(x.Artist), normalizedArtist))
                .ToList();

            if (exactArtistMatches.Count > 0 && exactArtistMatches.Count != result.Count)
            {
                _logger.Info($"Found {exactArtistMatches.Count} exact artist matches for '{artist}' out of {result.Count} in '{q}'");
                return exactArtistMatches;
            }

            return result;
        }

        private string NormalizeArtistName(string artist)
        {
            return StringProcessorRunner.Run(artist, new IStringProcessor[]
            {
                new RemoveDotsProcessor(),
                new LatintoAsciiProcessor(),
                new RemoveLeadingTheProcessor()
            });
        }
    }
}