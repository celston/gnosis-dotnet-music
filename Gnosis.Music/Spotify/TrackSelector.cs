﻿using System;
using System.Collections.Generic;

namespace Gnosis.Music.Spotify
{
    public class TrackSelector
    {
        WeightedRandomTrackSelector selector = new WeightedRandomTrackSelector();
        Random random = new Random();

        public string Select(string contextArtist, IList<SimplifiedSpotifyTrack> remoteTracks, IList<SpotifyLocalTrack> localTracks)
        {
            int remoteTrackCount = remoteTracks.Count;
            int localTrackCount = localTracks.Count;
            int totalTrackCount = remoteTrackCount + localTrackCount;

            if (totalTrackCount == 0)
            {
                throw new Exception("Remote and local tracks empty");
            }

            int r = random.Next(totalTrackCount);
            if (r < remoteTrackCount)
            {
                return selector.Select(contextArtist, remoteTracks).Uri;
            }

            r = random.Next(localTrackCount);
            if (r >= localTrackCount)
            {
                throw new Exception(string.Format("{0} >= {1}", r, localTrackCount));
            }
            return localTracks[r].Uri;
        }

        public List<string> Build(int count, string contextArtist, IList<SimplifiedSpotifyTrack> remoteTracks, IList<SpotifyLocalTrack> localTracks)
        {
            List<string> result = new List<string>();
            for (int i = 0; i < count; i++)
            {
                try
                {
                    result.Add(Select(contextArtist, remoteTracks, localTracks));
                }
                catch (Exception)
                {
                }
            }

            return result;
        }
    }
}