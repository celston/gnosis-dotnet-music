﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using SpotifyAPI.Web;

namespace Gnosis.Music.Spotify
{
    public class LocalTracksManager
    {
        private readonly SpotifyWebAPI _spotify;
        private readonly Lazy<IMongoCollection<SpotifyLocalTrack>> _lazyCollection = new Lazy<IMongoCollection<Music.SpotifyLocalTrack>>(() =>
        {
            return Utility.Database.GetCollection<SpotifyLocalTrack>("localtracks");
        });

        private IMongoCollection<SpotifyLocalTrack> Collection
        {
            get { return _lazyCollection.Value; }
        }
        
        public LocalTracksManager(SpotifyWebAPI spotify)
        {
            _spotify = spotify;
        }

        public List<SpotifyLocalTrack> GetAll()
        {
            return Collection.Find(new BsonDocument()).ToList();
        } 
        
        public void RefreshDatabase()
        {
            Collection.DeleteMany(new BsonDocument());

            var tracks = _spotify.GetPlaylistTracks(Constants.UserId, Constants.LocalTracksPlaylistId);

            while (true)
            {
                foreach (var item in tracks.Items)
                {
                    Collection.InsertOne(new SpotifyLocalTrack()
                    {
                        Artist = item.Track.Artists[0].Name,
                        Album = item.Track.Album.Name,
                        Name = item.Track.Name,
                        Uri = item.Track.Uri,
                        IsLocal = item.IsLocal
                    });
                }

                if (!tracks.HasNextPage())
                {
                    break;
                }

                tracks = _spotify.GetNextPage(tracks);
            }
        } 
    }
}