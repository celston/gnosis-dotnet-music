﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Gnosis.Music.LastFm;
using Gnosis.Music.Spotify;
using MongoDB.Bson;
using MongoDB.Driver;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Models;

namespace Gnosis.Music
{
    public class MixPlaylistUpdater
    {
        private readonly SpotifyWebAPI _spotify;
        private readonly SpotifyRemoteTrackSearcher _spotifyRemoteTrackSearcher;
        private readonly List<SpotifyLocalTrack> _allLocalTracks;
        private readonly TrackSelector _trackSelector = new TrackSelector();
        private readonly IMongoCollection<BsonDocument> _exceptionCollection;
        private readonly IMongoCollection<BsonDocument> _logCollection;
        private readonly List<string> _usedUris = new List<string>();
        private readonly ILogger _logger;

        public MixPlaylistUpdater(SpotifyWebAPI spotify, ILogger logger)
        {
            _spotify = spotify;
            _spotifyRemoteTrackSearcher = new SpotifyRemoteTrackSearcher(_spotify, logger);

            _logger = logger;

            LocalTracksManager localTracksManager = new LocalTracksManager(_spotify);
            _allLocalTracks = localTracksManager.GetAll();
            _exceptionCollection = Utility.Database.GetCollection<BsonDocument>("Exception");
            _exceptionCollection.DeleteMany(new BsonDocument());
            _logCollection = Utility.Database.GetCollection<BsonDocument>("Log");
            _logCollection.DeleteMany(new BsonDocument());
        }

        public void Update(int n = 100)
        {
            UserRecentTracksManager userRecentTracksManager = new UserRecentTracksManager(_logger);
            userRecentTracksManager.RefreshDatabase();
            //userRecentTracksManager.RebuildDatabase();

            List<UserRecentTrack> recentTracks = userRecentTracksManager.GetLocalRandom(DateTime.Today.AddDays(-60), n);
            List<UserRecentTrack> allTimeTracks = userRecentTracksManager.GetLocalRandom(new DateTime(2000, 1, 1), n);

            List<string> uris = new List<string>();

            for (int i = 0; i < n; i++)
            {
                var recentTrack = recentTracks[i];
                _logCollection.InsertOne(new BsonDocument()
                {
                    { "Message", "Recent Track"},
                    { "Artist", recentTrack.Artist },
                    { "Album", recentTrack.Album },
                    { "Name", recentTrack.Name },
                    { "Date", recentTrack.Date }
                });
                _logger.Debug($"Recent track {recentTrack.Artist} - {recentTrack.Album} - {recentTrack.Name} - {recentTrack.Date}");
                
                int artistTracks = 1;
                var buffer = new List<string>();
                try
                {
                    buffer.Add(FindSimilarByArtistAndName(recentTrack));
                }
                catch (Exception ex)
                {
                    _logger.Warn($"Failed FindSimilarByArtistAndName: {ex.Message}");
                    _exceptionCollection.InsertOne(new BsonDocument("Message", ex.Message));
                    artistTracks++;
                }
                try
                {
                    buffer.Add(FindSimilarByArtistAndAlbum(recentTrack));
                }
                catch (Exception ex)
                {
                    _logger.Warn($"Failed FindSimilarByArtistAndAlbum: {ex.Message}");
                    _exceptionCollection.InsertOne(new BsonDocument("Message", ex.Message));
                    artistTracks++;
                }
                try
                {
                    for (int j = 0; j < artistTracks; j++)
                    {
                        buffer.Add(FindSimilarByArtist(recentTrack));
                    }
                }
                catch (Exception ex)
                {
                    _logger.Warn($"Failed FindSimilarByArtist: {ex.Message}");
                    _exceptionCollection.InsertOne(new BsonDocument("Message", ex.Message));
                }

                if (buffer.Any())
                {
                    AddTracksToPlaylist(buffer);
                    buffer.Clear();
                }

                artistTracks = 1;
                var allTimeTrack = allTimeTracks[i];
                _logCollection.InsertOne(new BsonDocument()
                {
                    { "Message", "All Time Track"},
                    { "Artist", allTimeTrack.Artist },
                    { "Album", allTimeTrack.Album },
                    { "Name", allTimeTrack.Name },
                    { "Date", allTimeTrack.Date }
                });
                _logger.Debug($"All time track {allTimeTrack.Artist} - {allTimeTrack.Album} - {allTimeTrack.Name} - {allTimeTrack.Date}");

                try
                {
                    buffer.Add(FindSimilarByArtistAndName(allTimeTrack));
                }
                catch (Exception ex)
                {
                    _logger.Warn($"Failed FindSimilarByArtist: {ex.Message}");
                    _exceptionCollection.InsertOne(new BsonDocument("Message", ex.Message));
                    artistTracks++;
                }
                try
                {
                    buffer.Add(FindSimilarByArtistAndAlbum(allTimeTrack));
                }
                catch (Exception ex)
                {
                    _logger.Warn($"Failed FindSimilarByArtistAndAlbum: {ex.Message}");
                    _exceptionCollection.InsertOne(new BsonDocument("Message", ex.Message));
                    artistTracks++;
                }
                try
                {
                    for (int j = 0; j < artistTracks; j++)
                    {
                        buffer.Add(FindSimilarByArtist(allTimeTrack));
                    }
                }
                catch (Exception ex)
                {
                    _logger.Warn($"Failed FindSimilarByArtist: {ex.Message}");
                    _exceptionCollection.InsertOne(new BsonDocument("Message", ex.Message));
                }

                if (buffer.Any())
                {
                    AddTracksToPlaylist(buffer);
                    buffer.Clear();
                }
            }
        }

        private void AddTracksToPlaylist(List<string> uris)
        {
            ErrorResponse error = _spotify.AddPlaylistTracks(Spotify.Constants.UserId, Spotify.Constants.MixPlaylistId, uris);
            if (error.HasError())
            {
                _logger.Error($"Spotify AddPlaylistTracks error: {error.Error.Message}");
                throw new Exception(error.Error.Message);
            }
            Thread.Sleep(1000);
            _usedUris.AddRange(uris);
        }

        private void Validate(List<SimplifiedSpotifyTrack> remoteTracks, List<SpotifyLocalTrack> localTracks, string msg)
        {
            if (!remoteTracks.Any() && !localTracks.Any())
            {
                throw new Exception(msg);
            }
        }

        public string FindSimilarByArtistAndName(string artist, string name)
        {
            List<SimplifiedSpotifyTrack> remoteTracks = _spotifyRemoteTrackSearcher.Search(50, artist, name);
            if (remoteTracks.Any(x => x.IsExplicit))
            {
                int originalCount = remoteTracks.Count;
                remoteTracks = remoteTracks.Where(x => x.IsExplicit).ToList();
                _logger.Info($"Found {remoteTracks.Count} explicit vs. {originalCount} total tracks for {artist} - {name}");
            }
            List<SpotifyLocalTrack> localTracks = _allLocalTracks.Where(x => x.Artist.ToUpper() == artist && String.Equals(x.Name, name, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Validate(remoteTracks, localTracks, $"No tracks found for artist '{artist}' and name '{name}'");

            return Select(artist, remoteTracks, localTracks);
        }

        public string FindSimilarByArtistAndName(UserRecentTrack track)
        {
            return FindSimilarByArtistAndName(track.Artist, track.Name);
        }

        public string FindSimilarByArtistAndAlbum(UserRecentTrack track)
        {
            return FindSimilarByArtistAndAlbum(track.Artist, track.Album);
        } 

        public string FindSimilarByArtistAndAlbum(string artist, string album)
        {
            List<SimplifiedSpotifyTrack> remoteTracks = _spotifyRemoteTrackSearcher.Search(50, artist, null, album);
            List<SpotifyLocalTrack> localTracks = _allLocalTracks.Where(x => string.Equals(x.Artist, artist, StringComparison.CurrentCultureIgnoreCase) && string.Equals(x.Album, album, StringComparison.CurrentCultureIgnoreCase)).ToList();

            Validate(remoteTracks, localTracks, $"No tracks found for artist '{artist}' and album '{album}'");

            return Select(artist, remoteTracks, localTracks);
        }

        public string FindSimilarByArtist(UserRecentTrack track)
        {
            List<SimplifiedSpotifyTrack> remoteTracks = _spotifyRemoteTrackSearcher.Search(100, track.Artist);
            List<SpotifyLocalTrack> localTracks = _allLocalTracks.Where(x => x.Artist.ToUpper() == track.Artist.ToUpper()).ToList();

            Validate(remoteTracks, localTracks, $"No tracks found for artist '{track.Artist}'");

            return Select(track.Artist, remoteTracks, localTracks);
        }

        public string Select(string contextArtist, List<SimplifiedSpotifyTrack> remoteTracks, List<SpotifyLocalTrack> localTracks)
        {
            return _trackSelector.Select(contextArtist, remoteTracks.Where(x => !_usedUris.Contains(x.Uri)).ToList(), localTracks.Where(x => !_usedUris.Contains(x.Uri)).ToList());
        } 
    }
}