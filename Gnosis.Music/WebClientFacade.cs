﻿using System;
using System.Net;
using System.Runtime.InteropServices;

namespace Gnosis.Music
{
    public class WebClientFacade : IWebClientFacade
    {
        private readonly int _maxRetries;
        private readonly WebClient _webClient;
        private readonly ILogger _logger;

        public WebClientFacade(int maxRetries, ILogger logger)
        {
            _maxRetries = maxRetries;
            _webClient = new WebClient();
            _logger = logger;
        }

        public void Dispose()
        {
            _webClient?.Dispose();
        }

        public string DownloadString(string url)
        {
            return DownloadString(url, _maxRetries);
        }

        private string DownloadString(string url, int remainingTries)
        {
            try
            {
                return _webClient.DownloadString(url);
            }
            catch (Exception)
            {
                _logger.Warn($"Failed to download {url}, remaining tries {remainingTries}");
                if (remainingTries > 1)
                {
                    return DownloadString(url, remainingTries - 1);
                }

                throw;
            }
        }
    }
}