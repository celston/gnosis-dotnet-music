﻿using System;
using System.IO;

namespace Gnosis.Music
{
    public class FileLogger : ILogger
    {
        private readonly StreamWriter _streamWriter;

        public FileLogger()
        {
            _streamWriter = new StreamWriter($"log-{DateTime.Now:s}.txt".Replace(':', '-'));
        }

        public void Dispose()
        {
            _streamWriter.Dispose();
        }

        public void Debug(string msg)
        {
            WriteMessage("DEBUG", msg);
        }

        public void Info(string msg)
        {
            WriteMessage("INFO", msg);
        }

        public void Warn(string msg)
        {
            WriteMessage("WARN", msg);
        }

        public void Error(string msg)
        {
            WriteMessage("ERROR", msg);
        }

        private void WriteMessage(string tag, string msg)
        {
            _streamWriter.WriteLine($"[{tag}]\t[{DateTime.Now}\t{msg}");
        }
    }
}