﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using MongoDB.Driver;

namespace Gnosis.Music
{
    public class Utility
    {
        private static readonly Lazy<IMongoDatabase> LazyDatabase = new Lazy<IMongoDatabase>(() =>
        {
            MongoClient client = new MongoClient("mongodb://localhost:27017");
            return client.GetDatabase("lastfm");
        });

        public static IMongoDatabase Database => LazyDatabase.Value;

        public static List<string> GetLongestWords(string s, int count = 3)
        {
            string trimmed = s.Trim('.', ' ');
            string[] words = Regex.Split(trimmed, @"[^\.\w-]+");

            return words
                .Where(x => !string.IsNullOrEmpty(x))
                .Distinct()
                .OrderByDescending(x => x.Length)
                .Take(count)
                .ToList();
        }   
    }
}