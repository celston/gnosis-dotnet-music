﻿using System;

namespace Gnosis.Music
{
    public interface ILogger : IDisposable
    {
        void Debug(string msg);
        void Info(string msg);
        void Warn(string msg);
        void Error(string msg);
    }
}