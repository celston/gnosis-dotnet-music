﻿using System.Text.RegularExpressions;

namespace Gnosis.Music.Strings
{
    public class RemoveApostrophesProcessors : IStringProcessor
    {
        public string Process(string s)
        {
            return Regex.Replace(Regex.Replace(s, "’", ""), "'", "");
        }
    }
}