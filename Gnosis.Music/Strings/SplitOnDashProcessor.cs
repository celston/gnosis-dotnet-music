﻿using System.Text.RegularExpressions;

namespace Gnosis.Music.Strings
{
    public class SplitOnDashProcessor : IStringProcessor
    {
        public string Process(string s)
        {
            return Regex.Split(s, " - ")[0];
        }
    }
}