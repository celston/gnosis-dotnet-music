﻿using System.Text;
using System.Linq;

namespace Gnosis.Music.Strings
{
    public class LatintoAsciiProcessor : IStringProcessor
    {
        public string Process(string s)
        {
            var newStringBuilder = new StringBuilder();
            
            newStringBuilder
                .Append(s.Normalize(NormalizationForm.FormKD)
                .Where(x => x < 128)
                .ToArray());

            return newStringBuilder.ToString();
        }
    }
}