﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace Gnosis.Music.Strings
{
    public class StringProcessorRunner : IEnumerable<IStringProcessor>
    {
        private readonly List<IStringProcessor> _processors = new List<IStringProcessor>();

        public string Run(string s)
        {
            return Run(s, _processors);
        }

        public static string Run(string s, IEnumerable<IStringProcessor> processors)
        {
            return processors.Aggregate(s, (current, processor) => processor.Process(current));
        }

        public IEnumerator<IStringProcessor> GetEnumerator()
        {
            return _processors.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(IStringProcessor processor)
        {
            _processors.Add(processor);
        }
    }
}