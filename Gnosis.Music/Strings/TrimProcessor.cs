﻿namespace Gnosis.Music.Strings
{
    public class TrimProcessor : IStringProcessor
    {
        public string Process(string s)
        {
            return s.Trim();
        }
    }
}