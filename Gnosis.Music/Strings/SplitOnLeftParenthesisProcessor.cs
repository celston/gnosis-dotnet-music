﻿using System.Text.RegularExpressions;

namespace Gnosis.Music.Strings
{
    public class SplitOnLeftParenthesisProcessor : IStringProcessor
    {
        public string Process(string s)
        {
            return Regex.Split(s, @" \(")[0];
        }
    }
}