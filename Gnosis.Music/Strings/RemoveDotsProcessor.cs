﻿using System.Text.RegularExpressions;

namespace Gnosis.Music.Strings
{
    public class RemoveDotsProcessor : IStringProcessor
    {
        public string Process(string s)
        {
            return Regex.Replace(s, @"\.", "");
        }
    }
}