﻿using System.Globalization;

namespace Gnosis.Music.Strings
{
    public class TitleCaseProcessor : IStringProcessor
    {
        private static readonly TextInfo TextInfo = new CultureInfo("en-US", false).TextInfo;
        
        public string Process(string s)
        {
            return TextInfo.ToTitleCase(s);
        }
    }
}