﻿namespace Gnosis.Music.Strings
{
    public interface IStringProcessor
    {
        string Process(string s);
    }
}