﻿using System.Text.RegularExpressions;

namespace Gnosis.Music.Strings
{
    public class RemoveLeadingTheProcessor : IStringProcessor
    {
        public string Process(string s)
        {
            return Regex.Replace(s, "^The ", "", RegexOptions.IgnoreCase);
        }
    }
}