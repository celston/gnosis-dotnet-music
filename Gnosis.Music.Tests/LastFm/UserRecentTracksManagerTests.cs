﻿using System;
using System.Collections.Generic;
using Gnosis.Music.LastFm;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Gnosis.Music.Tests.LastFm
{
    [TestClass]
    public class UserRecentTracksManagerTests
    {
        private readonly UserRecentTracksManager _userRecentTracksManager = new UserRecentTracksManager();

        [TestMethod]
        public void Get_30DaysAgo_Pass()
        {    
            var result = _userRecentTracksManager.GetRemote(DateTime.Today.AddDays(-30));
        }

        [TestMethod]
        public void RebuildDatabase_Default_Pass()
        {
            _userRecentTracksManager.RebuildDatabase();
        }

        [TestMethod]
        public void RefreshDatabase_Default_Pass()
        {
            int count = _userRecentTracksManager.RefreshDatabase();
            Assert.AreEqual(0, count);
        }

        [TestMethod]
        public void GetLocal_30DaysAgo_Pass()
        {
            List<UserRecentTrack> tracks = _userRecentTracksManager.GetLocal(DateTime.Today.AddDays(-30));
        }

        [TestMethod]
        public void GetLocalRandom_30DaysAgo_Pass()
        {
            List<UserRecentTrack> tracks = _userRecentTracksManager.GetLocalRandom(DateTime.Today.AddDays(-30), 50);
        }
    }
}