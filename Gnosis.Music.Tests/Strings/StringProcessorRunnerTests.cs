﻿using Gnosis.Music.Strings;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Gnosis.Music.Tests.Strings
{
    [TestClass]
    public class StringProcessorRunnerTests
    {
        [TestMethod]
        public void Run_Valid_Pass()
        {
            StringProcessorRunner runner = new StringProcessorRunner
            {
                new RemoveLeadingTheProcessor(),
                new SplitOnDashProcessor(),
                new SplitOnLeftParenthesisProcessor(),
                new TitleCaseProcessor(),
                new TrimProcessor()
            };

            string s = runner.Run("the numbers game - something (live)");
            Assert.AreEqual("Numbers Game", s);
        }

        [TestMethod]
        public void RunStatic_Valid_Pass()
        {
            string s = StringProcessorRunner.Run("the numbers game ", new IStringProcessor[]
            {
                new RemoveLeadingTheProcessor(),
                new TitleCaseProcessor(),
                new TrimProcessor()
            });

            Assert.AreEqual("Numbers Game", s);
        }
    }
}