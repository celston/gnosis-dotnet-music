﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Helpers;
using System.Web.Razor.Text;
using System.Web.UI.WebControls;
using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Gnosis.Music.Tests
{
    [TestClass]
    public class UnitTest1
    {
        #region Private Fields

        private readonly DateTime _origin = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private static readonly Lazy<IMongoCollection<BsonDocument>> LazyRecentTracks = new Lazy<IMongoCollection<BsonDocument>>(() => Utility.Database.GetCollection<BsonDocument>("recenttracks"));
        private static readonly Lazy<IMongoCollection<BsonDocument>> LazyLocalTracks = new Lazy<IMongoCollection<BsonDocument>>(() => Utility.Database.GetCollection<BsonDocument>("localtracks"));
        private static readonly Lazy<Random> LazyRandom = new Lazy<Random>(() => new Random());

        #endregion

        #region Private Properties

        private static IMongoCollection<BsonDocument> RecentTracks
        {
            get { return LazyRecentTracks.Value; }
        }

        private static IMongoCollection<BsonDocument> LocalTracks
        {
            get { return LazyLocalTracks.Value; }
        }

        private static Random Random
        {
            get { return LazyRandom.Value; }
        }

        #endregion

        #region Nested Interfaces

        public interface IRecentTrack
        {
            string Artist { get; }
        }

        #endregion

        #region Nested Classes

        public class DynamicRecentTrackAdapter : IRecentTrack
        {
            private readonly dynamic _data;

            public DynamicRecentTrackAdapter(dynamic data)
            {
                _data = data;
            }

            public string Artist
            {
                get { return _data.artist; }
            }
        }

        #endregion

        [TestMethod]
        public void SpotifyAuthenticate()
        {
            
        }

        [TestMethod]
        public void SearchLocalFiles()
        {
            var collection = Utility.Database.GetCollection<SpotifyLocalTrack>("localtracks");
            var foo = collection.AsQueryable().Where(x => x.Artist == "Screeching Weasel").ToList();
        }

        private BsonRegularExpression CreateBsonRegex(string pattern)
        {
            return new BsonRegularExpression(new Regex(pattern, RegexOptions.IgnoreCase));
        }

        [TestMethod]
        public void ProcessLocals()
        {
            string[] locals = File.ReadAllLines(@"C:\users\celston\last.fm\local.txt");

            Regex remoteRegex = new Regex("open.spotify.com/track/(.+)");
            Regex localRegex = new Regex("local/([^/]+)/([^/]+)/([^/]+)");

            LocalTracks.DeleteMany(new BsonDocument());

            using (WebClient webClient = new WebClient())
            {
                foreach (string local in locals)
                {
                    Match m = remoteRegex.Match(local);
                    if (m.Success)
                    {
                        string url = "https://api.spotify.com/v1/tracks/" + m.Groups[1].Captures[0];
                        string json = webClient.DownloadString(url);
                        dynamic data = Json.Decode(json);

                        BsonDocument doc = CreateLocalTrackDoc(data);
                        LocalTracks.InsertOne(doc);
                        Thread.Sleep(250);
                    }
                }
            }
        }

        [TestMethod]
        public void AlbumsByArtist()
        {
            var filter = Builders<BsonDocument>.Filter.Eq("artist", "Bad Religion");
            var list = RecentTracks.Find(filter).ToList();
            var grouped = list.GroupBy(x => NormalizeAlbumName(x["album"].AsString));
            foreach (var grouping in grouped.OrderByDescending(x => x.Count()))
            {
                Debug.Print("{0}: {1}", grouping.Key, grouping.Count());
            }
        }

        [TestMethod]
        public void GetTwoLongestWords_FromRandomTracks_Pass()
        {
            for (var i = 0; i < 10; i++)
            {
                BsonDocument track = GetRandomRecentTrack();
                List<string> words = Utility.GetLongestWords(track["album"].AsString).ToList();
            }
        }

        [TestMethod]
        public void CopyLocalTracksForSpotify()
        {
            foreach (string filePath in Directory.GetFiles(@"F:\Music\Spotify"))
            {
                File.Delete(filePath);
            }

            using (WebClient webClient = new WebClient())
            using (StreamReader streamReader = File.OpenText(@"C:\Users\celston\last.fm\Music.txt"))
            using (CsvReader csvReader = new CsvReader(streamReader, new CsvConfiguration()
            {
                Delimiter = "\t"
            }))
            {
                int i = 0;
                while (csvReader.Read())
                {
                    Thread.Sleep(100);

                    string artist = csvReader.GetField<string>("Artist");
                    if (string.IsNullOrWhiteSpace(artist))
                    {
                        continue;
                    }
                    artist = Regex.Replace(artist, "^The ", "");

                    string album = csvReader.GetField<string>("Album");
                    album = Regex.Replace(album, "^The ", "");
                    string name = csvReader.GetField<string>("Name");

                    DynamicJsonArray results = SmartSpotifyTrackSearch(webClient, artist, album, name);

                    if (results.Any())
                    {
                        continue;
                    }

                    int? trackNumber = csvReader.GetField<int?>("Track Number");
                    if (!trackNumber.HasValue)
                    {
                        trackNumber = 0;
                    }
                    string location = csvReader.GetField<string>("Location");    
                    string extension = Path.GetExtension(location);

                    string destination = string.Format(@"F:\Music\Spotify\{0} - {1} - {2:00} - {3}{4}", RemoveInvalidCharacters(artist), RemoveInvalidCharacters(album), trackNumber, RemoveInvalidCharacters(name), extension);
                    if (File.Exists(destination))
                    {
                        continue;
                    }

                    File.Copy(location, destination);
                }
            } 
        }

        private string RemoveInvalidCharacters(string s)
        {
            return Regex.Replace(s, "[^a-zA-Z0-9 ]", "");
        }

        [TestMethod]
        public void Foo()
        {
            List<string> uris = new List<string>();
            
            using (WebClient webClient = new WebClient())
            {
                for (int i = 0; i < 100; i++)
                {
                    Thread.Sleep(100);

                    BsonDocument track = GetRandomRecentTrack();

                    string album = track["album"].AsString;
                    List<string> albumQueries = Utility.GetLongestWords(album).Select(x => "album:" + x).ToList();

                    string artist = track["artist"].AsString;
                    List<string> artistQueries = Utility.GetLongestWords(artist).Select(x => "artist:" + x).ToList();

                    string name = track["name"].AsString;
                    List<string> nameQueries = Utility.GetLongestWords(name).Select(x => "track:" + x).ToList();

                    dynamic results1 = SpotifyTrackSearch(webClient, artistQueries.Union(nameQueries));
                    string randomUri1 = GetWeightedRandomSpotifyTrack(results1.tracks.items);
                    if (randomUri1 != null)
                    {
                        uris.Add(randomUri1);
                    }

                    dynamic results2 = SpotifyTrackSearch(webClient, artistQueries.Union(albumQueries));
                    string randomUri2 = GetWeightedRandomSpotifyTrack(results2.tracks.items);
                    if (randomUri2 != null)
                    {
                        uris.Add(randomUri2);
                    }

                    dynamic results3 = SpotifyTrackSearch(webClient, artistQueries);
                    string randomUri3 = GetWeightedRandomSpotifyTrack(results3.tracks.items);
                    if (randomUri3 != null)
                    {
                        uris.Add(randomUri3);
                    }
                }
            }

            File.WriteAllLines(@"C:\Users\celston\last.fm\playlist.txt", uris);
        }

        [TestMethod]
        public void SmartSpotifyTrackSearch_Specific_Pass()
        {
            using (WebClient webClient = new WebClient())
            {
                dynamic result = SmartSpotifyTrackSearch(webClient, "AFI", "All Hallow's E.P.", "Fall Children");
            }
        }

        private dynamic SmartSpotifyTrackSearch(WebClient webClient, string artist = null, string album = null, string name = null)
        {
            List<string> queries = new List<string>();
            
            if (!string.IsNullOrWhiteSpace(artist))
            {
                queries.AddRange(Utility.GetLongestWords(artist).Select(x => "artist:" + x));
            }
            if (!string.IsNullOrWhiteSpace(album))
            {
                queries.AddRange(Utility.GetLongestWords(album).Select(x => "album:" + x));
            }
            if (!string.IsNullOrWhiteSpace(name))
            {
                queries.AddRange(Utility.GetLongestWords(name).Select(x => "track:" + x));
            }

            return SpotifyTrackSearch(webClient, queries).tracks.items;
        }

        private dynamic SpotifyTrackSearch(WebClient webClient, IEnumerable<string> queries)
        {
            return SpotifyTrackSearch(webClient, string.Join(" ", queries));
        }

        private dynamic SpotifyTrackSearch(WebClient webClient, string q)
        {
            string url = "https://api.spotify.com/v1/search?type=track&limit=50&q=" + q;

            string json = webClient.DownloadString(url);
            return Json.Decode(json);
        }

        private string GetWeightedRandomSpotifyTrack(dynamic[] items)
        {
            if (items.Length == 0)
            {
                return null;
            }
            if (items.Length == 1)
            {
                return items[0].uri;
            }
            
            double totalPopularity = items.Select(x => Math.Sqrt(((int) x.popularity) + 1)).Sum();
            double r = Random.NextDouble()*totalPopularity;

            foreach (dynamic item in items)
            {
                double popularity = Math.Sqrt(((int)item.popularity) + 1);
                if (r < popularity)
                {
                    return item.uri;
                }

                r -= popularity;
            }

            return items.Last().uri;
        }

        private BsonDocument GetRandomRecentTrack()
        {
            long count = RecentTracks.Count(new BsonDocument());

            int r = Random.Next((int)count);
            return RecentTracks.Find(new BsonDocument()).Skip(r).First();
        }

        private string NormalizeAlbumName(string original)
        {
            string[] bits = Regex.Split(original, @" \(");

            return bits[0];
        }

        [TestMethod]
        public void MaxDate()
        {
            var doc = RecentTracks.Find(new BsonDocument()).Sort(new BsonDocument("date", -1)).First();
            DateTime d = doc["date"].ToUniversalTime();
            TimeSpan diff = d - _origin;
            int x = (int) Math.Floor(diff.TotalSeconds);
        }

        [TestMethod]
        public void RefreshRecentTracks()
        {
            RecentTracks.DeleteMany(new BsonDocument());

            int page = 1;
            string urlFormat = "http://ws.audioscrobbler.com/2.0/?method=user.getRecentTracks&user={0}&api_key={1}&limit={2}&page={3}&format=json";
            int limit = 200;
            string dir = @"C:\Users\celston\last.fm\user.getRecentTracks";

            using (WebClient webClient = new WebClient())
            {
                string url = string.Format(urlFormat, Music.LastFm.Constants.Username, Music.LastFm.Constants.ApiKey, limit, page);
                string json = webClient.DownloadString(url);
                dynamic data = Json.Decode(json);

                int totalPages = int.Parse(data.recenttracks["@attr"].totalPages);

                foreach (dynamic track in data.recenttracks.track)
                {
                    RecentTracks.InsertOne(CreateDoc(track));
                }

                for (page = 2; page <= totalPages; page++)
                {
                    url = string.Format(urlFormat, Music.LastFm.Constants.Username, Music.LastFm.Constants.ApiKey, limit, page);
                    json = webClient.DownloadString(url);
                    data = Json.Decode(json);

                    foreach (dynamic track in data.recenttracks.track)
                    {
                        RecentTracks.InsertOne(CreateDoc(track));
                    }
                }
            }
        }

        private BsonDocument CreateLocalTrackDoc(dynamic track)
        {
            return new BsonDocument()
            {
                {"artist", track.artists[0].name},
                {"album", NormalizeAlbumName(track.album.name)},
                {"name", track.name},
                {"uri", track.uri}
            };
        }

        public BsonDocument CreateDoc(dynamic track)
        {
            long uts = long.Parse(track.date.uts);
            DateTime date = _origin.AddSeconds(uts);
            return new BsonDocument()
            {
                {"artist", track.artist["#text"]},
                {"name", track.name},
                {"album", track.album["#text"]},
                {"date", new BsonDateTime(date)}
            };
        }
    }
}
