﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpotifyWebAPI = SpotifyAPI.Web.SpotifyWebAPI;

namespace Gnosis.Music.Tests
{
    [TestClass]
    public class MixPlaylistUpdaterTests
    {
        [TestMethod]
        public void Update_5_Pass()
        {
            SpotifyAPI.Web.SpotifyWebAPI spotify = new SpotifyAPI.Web.SpotifyWebAPI()
            {
                TokenType = "Bearer",
                AccessToken = "BQAbsndYz980Jictc2vOlbS2DTAY_8Yt_LKisPtvpzWG2FJYcmGl45wTbRfveMnxOrNjW_k-3jd7UZBXiHG7dPkFdm_gCNiCoc1TGJEYbfpKejiMnh7XrpXsXQTcmUtlz_YjC4MoV-l_1Dl2PS3C6ajdZ3sjl1Fs7W2to9TBJNeLQhZ3VIDNvLYKfQfU40abXDxb0CZoORiM5z8xeP7g18uTV0RSxBT4KMvkVSJywJVVR66ScwSi6qAaDTh32902XGclVRTMESg2rogZ1N0nKv4hCEXVt44",
                UseAutoRetry = true
            };
            MixPlaylistUpdater mixPlaylistUpdater = new MixPlaylistUpdater(spotify);
            mixPlaylistUpdater.Update(50);
        }

        [TestMethod]
        public void FindSimilarByArtistAndAlbum_GaslightAnthemSenorAndTheQueen_Pass()
        {
            FindSimilarByArtistAndName("Gaslight Anthem", "Señor and the Queen");
        }

        [TestMethod]
        public void FindSimilarByArtistAndAlbum_BobSegerBackIn72_Pass()
        {
            FindSimilarByArtistAndAlbum("Bob Seger", "Back In '72");
        }

        private void FindSimilarByArtistAndName(string artist, string name)
        {
            SpotifyAPI.Web.SpotifyWebAPI spotify = new SpotifyAPI.Web.SpotifyWebAPI()
            {
                UseAuth = false
            };

            MixPlaylistUpdater mixPlaylistUpdater = new MixPlaylistUpdater(spotify);

            var result = mixPlaylistUpdater.FindSimilarByArtistAndName(artist, name);
        }

        private void FindSimilarByArtistAndAlbum(string artist, string album)
        {
            SpotifyAPI.Web.SpotifyWebAPI spotify = new SpotifyAPI.Web.SpotifyWebAPI()
            {
                UseAuth = false
            };

            MixPlaylistUpdater mixPlaylistUpdater = new MixPlaylistUpdater(spotify);

            var result = mixPlaylistUpdater.FindSimilarByArtistAndAlbum(artist, album);
        }
    }
}