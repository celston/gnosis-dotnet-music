﻿using Gnosis.Music.Spotify;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Gnosis.Music.Tests.Spotify
{
    [TestClass]
    public class WeightedRandomTrackSelectorTests
    {
        private readonly SpotifyAPI.Web.SpotifyWebAPI _spotify;
        private readonly SpotifyRemoteTrackSearcher _spotifyTrackSearcher;

        public WeightedRandomTrackSelectorTests()
        {
            _spotify = new SpotifyAPI.Web.SpotifyWebAPI
            {
                UseAuth = false
            };
            _spotifyTrackSearcher = new SpotifyRemoteTrackSearcher(_spotify);
        }

        [TestMethod]
        public void Search_TheAfghanWhigs_Pass()
        {
            var results = _spotifyTrackSearcher.Search(10, "The Afghan Whigs");
            WeightedRandomTrackSelector selector = new WeightedRandomTrackSelector();

            for (int i = 0; i < 5; i++)
            {
                var track = selector.Select("The Afghan Whigs", results);
            }
        } 
    }
}