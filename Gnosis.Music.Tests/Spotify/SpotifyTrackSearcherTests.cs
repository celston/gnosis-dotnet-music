﻿using System.Collections.Generic;
using System.Linq;
using Gnosis.Music.Spotify;
using Gnosis.Music.Strings;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Gnosis.Music.Tests.Spotify
{
    [TestClass]
    public class SpotifyTrackSearcherTests
    {
        private readonly SpotifyAPI.Web.SpotifyWebAPI _spotify;
        private readonly SpotifyRemoteTrackSearcher _spotifyTrackSearcher;

        public SpotifyTrackSearcherTests()
        {
            _spotify = new SpotifyAPI.Web.SpotifyWebAPI
            {
                UseAuth = false
            };
            _spotifyTrackSearcher = new SpotifyRemoteTrackSearcher(_spotify);
        }

        [TestMethod]
        public void Search_TheAfghanWhigs_Pass()
        {
            var results = _spotifyTrackSearcher.Search(10, "The Afghan Whigs", "Honky's Ladder");
        }

        [TestMethod]
        public void Search_ScreechingWeasel_Pass()
        {
            var results = _spotifyTrackSearcher.Search(10, "Screeching Weasel");
        }

        [TestMethod]
        public void Search_7Seconds_Pass()
        {
            var results = _spotifyTrackSearcher.Search(int.MaxValue, "7 Seconds");
        }
    }
}