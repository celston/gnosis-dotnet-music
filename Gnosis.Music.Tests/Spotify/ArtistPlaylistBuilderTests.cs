﻿using System.Collections.Generic;
using System.Linq;
using Gnosis.Music.Spotify;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;

namespace Gnosis.Music.Tests.Spotify
{
    [TestClass]
    public class ArtistPlaylistBuilderTests
    {
        private readonly SpotifyAPI.Web.SpotifyWebAPI _spotify;
        private readonly SpotifyRemoteTrackSearcher _spotifyTrackSearcher;
        private readonly TrackSelector _artistPlaylistBuilder = new TrackSelector();
        private readonly IMongoCollection<SpotifyLocalTrack> _collection;
            
        public ArtistPlaylistBuilderTests()
        {
            _collection = Utility.Database.GetCollection<SpotifyLocalTrack>("localtracks");
            
            _spotify = new SpotifyAPI.Web.SpotifyWebAPI
            {
                UseAuth = false
            };
            _spotifyTrackSearcher = new SpotifyRemoteTrackSearcher(_spotify);
        }

        [TestMethod]
        public void Build_ScreechingWeasel_Pass()
        {
            List<SimplifiedSpotifyTrack> remoteTracks = _spotifyTrackSearcher.Search(10, "Screeching Weasel");
            List<SpotifyLocalTrack> localTracks = _collection.AsQueryable().Where(x => x.Artist == "Screeching Weasel").ToList();

            List<string> result = _artistPlaylistBuilder.Build(100, "Screeching Weasel", remoteTracks, localTracks);
        }

        [TestMethod]
        public void Build_OneLocal_Pass()
        {
            List<SpotifyLocalTrack> localTracks = _collection.AsQueryable().Where(x => x.Artist == "Screeching Weasel").ToList();

            List<string> result = _artistPlaylistBuilder.Build(100, "Screeching Weasel", new List<SimplifiedSpotifyTrack>(), localTracks.Take(1).ToList());
        }

        [TestMethod]
        public void Build_TwoLocal_Pass()
        {
            List<SpotifyLocalTrack> localTracks = _collection.AsQueryable().Where(x => x.Artist == "Screeching Weasel").ToList();

            List<string> result = _artistPlaylistBuilder.Build(100, "Screeching Weasel", new List<SimplifiedSpotifyTrack>(), localTracks.Take(2).ToList());
        }

        [TestMethod]
        public void Build_AllRemote_Pass()
        {
            List<SimplifiedSpotifyTrack> remoteTracks = _spotifyTrackSearcher.Search(10, "Screeching Weasel");

            List<string> result = _artistPlaylistBuilder.Build(100, "Screeching Weasel", remoteTracks, new List<SpotifyLocalTrack>());
        }

        [TestMethod]
        public void Build_OneRemote_Pass()
        {
            List<SimplifiedSpotifyTrack> remoteTracks = _spotifyTrackSearcher.Search(10, "Screeching Weasel").Take(1).ToList();

            List<string> result = _artistPlaylistBuilder.Build(100, "Screeching Weasel", remoteTracks, new List<SpotifyLocalTrack>());
        }

        [TestMethod]
        public void Build_TwoRemote_Pass()
        {
            List<SimplifiedSpotifyTrack> remoteTracks = _spotifyTrackSearcher.Search(10, "Screeching Weasel").Take(2).ToList();

            List<string> result = _artistPlaylistBuilder.Build(100, "Screeching Weasel", remoteTracks, new List<SpotifyLocalTrack>());
        }
    }
}