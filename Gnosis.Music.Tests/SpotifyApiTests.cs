﻿using System;
using System.Text.RegularExpressions;
using Gnosis.Music.Spotify;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Driver;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Auth;
using SpotifyAPI.Web.Enums;
using SpotifyAPI.Web.Models;

namespace Gnosis.Music.Tests
{
    [TestClass]
    public class SpotifyApiTests
    {
        private static readonly Lazy<IMongoDatabase> LazyDatabase = new Lazy<IMongoDatabase>(() =>
        {
            MongoClient client = new MongoClient("mongodb://localhost:27017");
            return client.GetDatabase("lastfm");
        });

        private static IMongoDatabase Database
        {
            get { return LazyDatabase.Value; }
        }

        private static readonly Lazy<IMongoCollection<SpotifyLocalTrack>> LazyLocalTracks = new Lazy<IMongoCollection<SpotifyLocalTrack>>(() => Database.GetCollection<SpotifyLocalTrack>("localtracks"));

        private static IMongoCollection<SpotifyLocalTrack> LocalTracks
        {
            get { return LazyLocalTracks.Value; }
        }

        [TestMethod]
        public void ClientCredentialsAuth()
        {
            var auth = new ClientCredentialsAuth()
            {
                ClientId = Constants.ClientId,
                ClientSecret = Constants.ClientSecret,
                Scope = Scope.UserReadPrivate,
            };
            //With this token object, we now can make calls
            Token token = auth.DoAuth();
            var spotify = new SpotifyAPI.Web.SpotifyWebAPI()
            {
                TokenType = token.TokenType,
                AccessToken = token.AccessToken,
                UseAuth = false
            };
        }

        [TestMethod]
        public void SearchTracks()
        {
            var spotify = GetSpotify();

            var result = spotify.SearchItems("artist:Screeching artist:Weasel", SearchType.Track, 50);
        }

        [TestMethod]
        public void GetUserPlaylists()
        {
            var spotify = GetSpotify();

            var playlists = spotify.GetUserPlaylists(Constants.UserId, Int32.MaxValue);
        }

        [TestMethod]
        public void CopyFromLocalPlaylist()
        {
            var spotify = GetSpotify();

            var tracks = spotify.GetPlaylistTracks(Constants.UserId, Constants.LocalTracksPlaylistId);
            while (true)
            {
                foreach (var item in tracks.Items)
                {
                    string uri = item.Track.Uri;
                    if (Regex.IsMatch(uri, "spotify:local:"))
                    {
                        uri = Regex.Replace(uri, @":\d+$", "");    
                    }
                    
                    var result = spotify.AddPlaylistTrack(Constants.UserId, "5VPRvhDhGLAAt3umx9K7lL", uri);
                    if (result.HasError())
                    {
                        
                    }
                }

                if (!tracks.HasNextPage())
                {
                    break;
                }

                tracks = spotify.GetNextPage(tracks);
            }
        }

        [TestMethod]
        public void AddLocalPlaylistTrack()
        {
            var spotify = GetSpotify();

            var result = spotify.AddPlaylistTrack(Gnosis.Music.Spotify.Constants.UserId, "5VPRvhDhGLAAt3umx9K7lL", "https://open.spotify.com/local/Screeching%20Weasel/Baby%20Fat%20Act%201/Tower%20Of%20Talent/45");
        }

        private SpotifyAPI.Web.SpotifyWebAPI GetSpotify()
        {
            return new SpotifyAPI.Web.SpotifyWebAPI()
            {
                TokenType = "Bearer",
                AccessToken = "BQAYrXHJZTICt9J76-GA_bCBTMdSWaW1T0nNXOry70_s_8iBkXk69iorRLwfvzXn0cv7YJvFtLtpTq_mmkVy_Cla381wx2i6awRUJuAJACuSCV2AJdaveo461OLYNMsnlBlBDyZz5aMf1A7rZTFWP1yg4GcW6kcsDwMsqIU1aoM5PDnrFmD4wr62-0N1pCSKQWaR1X7nN0JPGajlygtuYUa89aRZND9dIJ0WfSpKRdvQQrOWRUNTCdK4Zs_ahji5rfApeBoqceuYF0AfeqMiD8kJEsFE4fc",
                UseAutoRetry = true
            };
        }
    }
}